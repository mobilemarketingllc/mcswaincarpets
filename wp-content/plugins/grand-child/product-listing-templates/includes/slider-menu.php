<table class="slider-menu">
	<tr>
		<td>
			<ul>
				<?php 
				$menu=get_field("slider_menu","options");
				foreach($menu as $option){
					?>
					<li>
						<span class="icon">
							<i class="fa fa-<?php echo $option["icon"] ?>"></i>
						</span>
						<a href="<?php echo $option["option_link"] ?>" class="padding"><?php echo $option["option_name"] ?></a>
					</li>
					<?php 
				}
				?>
			</ul>
		</td>
	</tr>
</table>