<div class="fr_toggle_box dir_bottom" data-dir="bottom">
	<div class="bg"></div>
	<div class="box_content">
		<div class="handle">
			Special Offers <i class="fa fa-chevron-down right"></i>
		</div>
		<div class="padding">
            <?php echo do_shortcode('[fl_builder_insert_layout slug="special-offers-post-list"]') ?>
			<?php /*?>
            <table>
				<tr>
					<?php 
					$boxes=get_field("fixed_bottom_box","options");
					foreach($boxes as $box){ ?>
						<td>
							<div data-fr-link="<?php echo $box["link"] ?>" class="box box_<?php echo $box["type"] ?>" style="<?php if($box["background_image"]){ ?>background-image:url(<?php echo fr_img($box["background_image"],"box",1) ?>)<?php } ?>;">
								<div class="text">
									<?php echo $box["content"] ?>&nbsp;
								</div>
								<?php if($box["button_text"]){ ?>
									<div class="fixed_bottom">
										<a href="<?php echo $box["link"] ?>" class="fl-button"><?php echo $box["button_text"] ?></a>
									</div>
								<?php } ?>
							</div>
						</td>
						<?php 
					}
					?>
				</tr>
			</table>
            <?php*/ ?>
		</div>
		</div>
	</div>
</div>