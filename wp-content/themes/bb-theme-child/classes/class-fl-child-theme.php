<?php

/**
 * Helper class for child theme functions.
 *
 * @class FLChildTheme
 */
final class FLChildTheme {
    
    /**
	 * Enqueues scripts and styles.
	 *
     * @return void
     */
    static public function enqueue_scripts()
    {
	    wp_enqueue_style( 'fl-child-theme', FL_CHILD_THEME_URL . '/style.css' );
    }
static public function archive_nav() {
		global $wp_query;

		if ( function_exists( 'wp_pagenavi' ) ) {
			wp_pagenavi();
		} elseif ( $wp_query->max_num_pages > 1 ) {
			echo '<nav class="fl-archive-nav clearfix">';
			echo '<div class="fl-archive-nav-prev">' . get_previous_posts_link( __( '&laquo; LOAD PREVIOUS', 'fl-automator' ) ) . '</div>';
			echo '<div class="fl-archive-nav-next">' . get_next_posts_link( __( 'MORE RESULTS &raquo;', 'fl-automator' ) ) . '</div>';
			echo '</nav>';
		}
	}
}