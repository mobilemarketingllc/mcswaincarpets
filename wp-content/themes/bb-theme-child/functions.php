<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Theme Setting Option added for backend
//acf_add_options_page( array("page_title"=>"Theme Settings") );

// Toggle Boc at footer
add_action('wp_footer', 'your_function_name');
function your_function_name(){
    get_template_part("includes/bottom-toggle-box");
}

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '1.12.4' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


/* Career */
function career_display()
{

    $career_html = '<div class="table-responsive"><table class="responsive careersTable table table-bordered" style="border: 1px solid #efefef; width: 100%; padding-top: 10px; padding-bottom: 10px;">
    <tbody>
    <tr style="padding-top: 5px; padding-bottom: 5px;">
		<th width="16%">Position</th>
		<th width="10%">Type</th>
		<th class="tableDescription" width="50%">Description</th>
		<th width="7%">Location</th>
		<th width="7%">Salary</th>
		<th width="10%">Action</th>
    </tr>';
    $args = array(  
        'post_type' => 'career_type',
        'post_status' => 'publish',
        
    );

    $loop = new WP_Query( $args ); 
        
    while ( $loop->have_posts() ) : $loop->the_post(); 
    $career_html .= '<tr style="padding-top: 10px; padding-bottom: 10px; background-color: #efefef;" data-career="'.get_the_title().'" >
    <td>'.get_the_title().'</td>
    <td>'.get_field('job_type').'</td>
    <td class="tableDescription">'.get_the_content().'</td>
    <td>'.get_field('job_location').'</td>
    <td>'.get_field('salary').'</td>
    <td><a href="javascript:void(0);" class="applyposiiton"  data-title="'.get_the_title().'" onclick="positionAppend(\''.get_the_title().'\')"><strong>Apply Now</strong></a></td>
    </tr>';
    endwhile;

    wp_reset_postdata(); 
    
    
    $career_html .= '</tbody>
    </table> </div>';
    return $career_html;

} 
add_shortcode('careers_shortcode', 'career_display');

add_filter( 'gform_pre_render_16', 'populate_specials' );
add_filter( 'gform_pre_validation_16', 'populate_specials' );
add_filter( 'gform_pre_submission_filter_16', 'populate_specials' );
add_filter( 'gform_admin_pre_render_16', 'populate_specials' );
function populate_specials( $form ) {
    foreach ( $form['fields'] as $field ) {
        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate_specials' ) === false ) {
            continue;
        }
        global $posts;
        $id = $posts->ID;
        $tags = get_the_tags($id);
        
        if($tags){
            $i=0;
            foreach ( $field->choices as $choice ) {
                foreach($tags as $tag) {
                   // var_dump($choice , strtolower($choice['value']));
                    if(strtolower($tag->slug) == strtolower($choice['value']) ){
                       $field->choices[$i]['isSelected'] = true;
                      // var_dump($field->choices[$i]);
                    }
                }
                $i++;
            }    
        }
        //var_dump($field->choices);
    }
    return $form;
}
add_filter( 'gform_field_value_specialProductName', 'populate_specialProductName' );
function populate_specialProductName( $value ) {   
    global $posts;

    return get_the_title($posts->ID);
}